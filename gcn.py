import os.path as osp
import argparse

import torch
import torch.nn.functional as F
from torch_geometric.datasets import Planetoid
import torch_geometric.transforms as T
from torch_geometric.nn import GCNConv, ChebConv  # noqa

parser = argparse.ArgumentParser()
print(parser)
parser.add_argument('--use_gdc', action='store_true',
                    help='Use GDC preprocessing.')
print(parser)
args = parser.parse_args()
print(args)

dataset = 'Cora'
path = osp.join(osp.dirname(osp.realpath(__file__)), '..', 'data', dataset)
dataset = Planetoid(path, dataset, T.NormalizeFeatures())
data = dataset[0]
print(data)
print(data.train_mask)
print(type(data.train_mask))
print(data.x)


print(data.x[100])
print(data.x[100].sum().item())
print(data.train_mask.dtype)
print(data.y.dtype)
print(data.y)



print(args.use_gdc)

if args.use_gdc:
    gdc = T.GDC(self_loop_weight=1, normalization_in='sym',
                normalization_out='col',
                diffusion_kwargs=dict(method='ppr', alpha=0.05),
                sparsification_kwargs=dict(method='topk', k=128,
                                           dim=0), exact=True)
    data = gdc(data)


class Net(torch.nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = GCNConv(dataset.num_features, 16, cached=True,normalize=not args.use_gdc)
        self.conv2 = GCNConv(16, dataset.num_classes, cached=True,normalize=not args.use_gdc)
        # self.conv1 = ChebConv(data.num_features, 16, K=2)
        # self.conv2 = ChebConv(16, data.num_features, K=2)

        self.reg_params = self.conv1.parameters()
        self.non_reg_params = self.conv2.parameters()

    def forward(self):
        x, edge_index, edge_weight = data.x, data.edge_index, data.edge_attr
        x = F.relu(self.conv1(x, edge_index, edge_weight))
        x = F.dropout(x, training=self.training)
        x = self.conv2(x, edge_index, edge_weight)
        return F.log_softmax(x, dim=1)


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model, data = Net().to(device), data.to(device)
optimizer = torch.optim.Adam([
    dict(params=model.reg_params, weight_decay=5e-4),
    dict(params=model.non_reg_params, weight_decay=0)
], lr=0.01)


def train():
    model.train()
    optimizer.zero_grad()
    F.nll_loss(model()[data.train_mask], data.y[data.train_mask]).backward()
    optimizer.step()


def test():
    model.eval()
    logits, accs = model(), []
    for _, mask in data('train_mask', 'val_mask', 'test_mask'):
        pred = logits[mask].max(1)[1]
        acc = pred.eq(data.y[mask]).sum().item() / mask.sum().item()
        accs.append(acc)
    return accs


best_val_acc = test_acc = 0
#for epoch in range(1, 201):
for epoch in range(1, 2):
    train()
    train_acc, val_acc, tmp_test_acc = test()
    if val_acc > best_val_acc:
        best_val_acc = val_acc
        test_acc = tmp_test_acc
    log = 'Epoch: {:03d}, Train: {:.4f}, Val: {:.4f}, Test: {:.4f}'
    print(log.format(epoch, train_acc, best_val_acc, test_acc))



'''
Traceback (most recent call last):
  File "gcn.py", line 53, in <module>
    model, data = Net().to(device), data.to(device)
  File "gcn.py", line 35, in __init__
    normalize=not args.use_gdc)
  File "/Users/apple/opt/anaconda3/lib/python3.7/site-packages/torch_geometric/nn/conv/gcn_conv.py", line 43, in __init__
    super(GCNConv, self).__init__(aggr='add', **kwargs)
TypeError: __init__() got an unexpected keyword argument 'normalize'
(base) mtec-vpn-013:RGCNcode apple$ 

Traceback (most recent call last):
  File "gcn.py", line 84, in <module>
    train()
  File "gcn.py", line 67, in train
    F.nll_loss(model()[data.train_mask], data.y[data.train_mask]).backward()
  File "/Users/apple/opt/anaconda3/lib/python3.7/site-packages/torch/nn/modules/module.py", line 541, in __call__
    result = self.forward(*input, **kwargs)
  File "gcn.py", line 50, in forward
    x = F.relu(self.conv1(x, edge_index, edge_weight))
  File "/Users/apple/opt/anaconda3/lib/python3.7/site-packages/torch/nn/modules/module.py", line 541, in __call__
    result = self.forward(*input, **kwargs)
  File "/Users/apple/opt/anaconda3/lib/python3.7/site-packages/torch_geometric/nn/conv/gcn_conv.py", line 97, in forward
    edge_index, norm = self.norm(edge_index, x.size(self.node_dim),
  File "/Users/apple/opt/anaconda3/lib/python3.7/site-packages/torch/nn/modules/module.py", line 585, in __getattr__
    type(self).__name__, name))
AttributeError: 'GCNConv' object has no attribute 'node_dim'
(base) mtec-vpn-013:RGCNcode apple$ 
'''



