import networkx as nx
import psycopg2,time
import pandas as pd


g=nx.Graph()

conn = psycopg2.connect(host="127.0.0.1",database="shared", user="jinzhang", password="94151q2w3e4r", port="5432")
cur = conn.cursor()
#sql="ALTER TABLE open_shapes.zurich ALTER COLUMN geom SET DATA TYPE geometry; ALTER TABLE open_shapes.zurich ALTER COLUMN geom SET DATA TYPE geometry(LineString) USING ST_LineMerge(geom)"
sql="""SELECT road_id, intersection_id
	   FROM open_shapes.zurich_intersection2road
	   ORDER BY road_id ASC;
	"""
cur.execute(sql)
row=cur.fetchall()

n=list(range(1,87591)) #node id from number of nodes
g.add_nodes_from(n)

tb=pd.DataFrame.from_records(row,columns=['road','inter'])
for i in range(0,len(tb),2):
    if tb.iloc[i,0]==tb.iloc[i+1,0]:
        st=int(tb.iloc[i,1])
        ed=int(tb.iloc[i+1,1])
        rid=int(tb.iloc[i,0])
        g.add_edge(st,ed,road_id=rid)
"""
for n in [2,20,200,2000]:
    current_time = time.time()
    bet_cen=nx.edge_betweenness_centrality(g,k=n, normalized=True, weight=None, seed=68)
    print(time.time()-current_time)
"""
#bet_cen=nx.edge_betweenness_centrality(g,normalized=True, weight=None, k=10, seed=68)
bet_cen=nx.edge_betweenness_centrality(g,normalized=True, weight=None)

bet_list=[]
for st,ed,rid in g.edges(data='road_id'):
    bet=bet_cen[st,ed] # no weight
    bet_list.append((rid,bet))

bet_list.sort()

print("listlength:",len(bet_list))
print(bet_list[0:11])


for index in bet_list:
    cur.execute("INSERT into open_shapes.zurich_attr(road_id,betweenness_centrality) VALUES (%s, %s)", index)

#sql=""
#cur.execute(sql)
conn.commit()
conn.close()

