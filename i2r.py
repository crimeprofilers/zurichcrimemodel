#import networkx as nx
#import matplotlib.pyplot as plt
import psycopg2
import pandas as pd

inter2road=pd.read_csv('intersection2road.csv')

i2r=[]
for i in range(0,len(inter2road)):
    i2r.append((int(inter2road.iat[i,0]),int(inter2road.iat[i,1])))
#i2r[0:4]

conn = psycopg2.connect(host="127.0.0.1",database="shared", user="jinzhang", password="94151q2w3e4r", port="5432")
cur = conn.cursor()

for index in i2r:
    cur.execute("INSERT into open_shapes.zurich_intersection2road(intersection_id,road_id) VALUES (%s, %s)", index)

conn.commit()
conn.close()