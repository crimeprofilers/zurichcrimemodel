import networkx as nx
import psycopg2,time
import pandas as pd

g=nx.Graph()

conn = psycopg2.connect(host="127.0.0.1",database="shared", user="jinzhang", password="94151q2w3e4r", port="5432")
cur = conn.cursor()
#sql="ALTER TABLE open_shapes.zurich ALTER COLUMN geom SET DATA TYPE geometry; ALTER TABLE open_shapes.zurich ALTER COLUMN geom SET DATA TYPE geometry(LineString) USING ST_LineMerge(geom)"
sql="""SELECT gid,ST_Startpoint(geom),ST_Endpoint(geom),ST_Length(geom::geography)
	   FROM open_shapes.zurich_road
	   ORDER BY gid DESC
    """
cur.execute(sql)
row=cur.fetchall()

#cur.execute("tianjiabianliang")

tb=pd.DataFrame.from_records(row,columns=['gid','start','end','length'],coerce_float=True)

nodelist=list(tb['start'])+list(tb['end'])
nodelist=list(set(nodelist))

#build g with parall
g_multi=nx.MultiGraph()
g_multi.add_nodes_from(nodelist)

"""
for i in range(0,len(tb)):
	rid=tb.iloc[i,0]
	st=tb.iloc[i,1]
	ed=tb.iloc[i,2]
	lent=tb.iloc[i,3]
	g.add_edge(st,ed,gid=rid,length=lent)
"""
for gid,start,end,length in row:
	g_multi.add_edge(start,end,gid=gid,length=length)

print("g_multi generated")

g_sim=nx.Graph()
g_sim.add_nodes_from(nodelist)
replace=[]
for u,v,keys,weight in g_multi.edges(data=True, keys=True):
    gid=weight['gid']
    l=weight['length']
    if (u,v) in list(g_sim.edges()):
        #print(g_sim[u][v])
        if g_sim[u][v]['length']>l: #only keep the shortest road
            old=g_sim[u][v]['gid']
            replace.append(old)
            g_sim[u][v]['length']=l
            g_sim[u][v]['gid']=gid
        else: replace.append(gid)
    else:
        g_sim.add_edge(u,v,gid=gid,length=l)
print("g_sim generated")
print("we have",len(replace),"parallel edges")
print(replace)


bet_w=nx.edge_betweenness_centrality(g_sim,k=None,weight='length')
bet=nx.edge_betweenness_centrality(g_sim,k=None)

print("betweenness calculated")

bet_list=[]
for u,v,gid in g_sim.edges(data='gid'):
    bet1=bet[u,v] # no weight
    bet2=bet_w[u,v] #weighted
    bet_list.append((gid,bet1,bet2))

for u,v,keys,gid in g_multi.edges(data='gid',keys=True):
    if gid in replace:
        bet1=bet[u,v]
        bet2=0
        bet_list.append((gid,bet1,bet2))

print("listlength:",len(bet_list))
print(bet_list[0:11])



for index in bet_list:
    cur.execute("INSERT into open_shapes.zurich_roadattr(road_id,betweenness_centrality,betweenness_centrality_w) VALUES (%s, %s, %s)", index)

conn.commit()    
conn.close()






