#billing??
#select cornor lat/lng(lat:y, lng:x)
#left-bottom:47.3176006170384866,8.4482933194036445
#right-bottom:47.3176006170384866,8.6254025463202399
#right-top:47.4356797021342302,8.6254025463202399
#left_top:47.4356797021342302,8.4482933194036445
#devided into 50-meter grids, record the lat/lng of each grid center
#make nearby search of each grid, location is the center, radio is 71
#返回一个dict，keys有html_attributions(空),results(list), status('OK','ZERO_RESULTS', quotalimitexceed?这个是报错，要用exception什么的),可能有next_page_token
#如果有next_page_token，说明有溢出，记录下这个grid的id，什么都不做
#如果是zero，直接跳过
#如果一切正常，是'OK'，采集下results里的数据：place_id, geometry/location，name,types, price_level, rating, vicinity:
#	list=['place_id', 'geometry'/'location，name',types, price_level, rating, vicinity]
#	for i in list:
#		if i in result dict: 对应的值加入dataframe, 不然就存成None？
#结束
#注意处理报错

import googlemaps, shapely, fiona
import math
import traceback,time
import pandas as pd
from math import radians, cos, sin, asin, sqrt 
from geopy import distance
from datetime import datetime
from shapely.geometry import shape,Point

def Haversine(lat1, lon1, lat2, lon2): 
    # 将十进制度数转化为弧度  
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])  
    # Haversine公式  
    dlon = lon2 - lon1   
    dlat = lat2 - lat1   
    a = sin(dlat/2.0)**2.0 + cos(lat1) * cos(lat2) * sin(dlon/2.0)**2.0  
    c = 2.0 * asin(sqrt(a))   
    r = 6371.0 # 地球平均半径，单位为公里  
    return round(c * r,3)

def distanceGeodesic(lat1, lon1, lat2, lon2): # google geometry library use geodesic distance
	return distance.distance((lat1, lon1), (lat2, lon2)).km

def distanceCircle(lat1, lon1, lat2, lon2):
	return distance.great_circle((lat1, lon1), (lat2, lon2)).km



def clientSet(keyvalue):
	return googlemaps.Client(key=keyvalue)

def generateCentroids(left,bottom,right,top,radius):
	#8.4482933194036445,47.3176006170384866,8.6254025463202399,47.4356797021342302,200
	interval=radius/1.41421356237
	xdist=math.ceil(distanceGeodesic(bottom,left,bottom,right)*1000.0) #x轴上的直线距离
	ydist=math.ceil(distanceGeodesic(top,left,bottom,left)*1000.0)#y轴上的直线距离
	num_x=math.ceil(xdist/(interval*2.0))#x轴上可以分成num_x个100米的格子
	num_y=math.ceil(ydist/(interval*2.0))#y轴上可以分成num_y个100米的格子
	print(xdist,ydist,num_x,num_y)
	x_grid=(right-left)*((interval*2.0)/xdist) #lng上从left每格增加多少度
	y_grid=(top-bottom)*((interval*2.0)/ydist) #lat上从bottom每格增加多少度
	print(x_grid,y_grid)
	print(left+num_x*x_grid-right)
	centroids=[]
	for i in range(0,num_y+1): #from top to bottom
		for k in range(0,num_x+1): #from left to right
			centroids.append((i,k,top-i*y_grid,left+k*x_grid)) #(row,column,lat,lng) of centroid
	return centroids

"""
def getCity():
	shp = fiona.open("/Users/apple/Desktop/mapdata/city_boundary/city_boundary.shp")
	first=shp.next()
	return shape(first['geometry'])
"""

def cleanCentroids(self,area):
	for i,j,lat,lng in self:
		if area.disjoint(Point(lng,lat)):
			self.remove((i,j,lat,lng))
	return self

def getPoi(client,l):
    print(l['status'])
    if l['status']=='OK':
        print(l['results'],file=f)
        print(i,j)
        print(l['results'][0])
    if not('next_page_token' in l.keys()):
        return
    getPoi(gmaps,gmaps.places_nearby(page_token=l['next_page_token']))


def getPoiModi(client,l):
    print(i,j,l['status'],client)
    if l['status']=='OK':
        print(l['results'],file=f)
        print(l['results'][0]['name'])
    if not('next_page_token' in l.keys()):
        return
    time.sleep(2)
    p=client.places_nearby(page_token=l['next_page_token'])
    time.sleep(2)
    getPoiModi(client,p)



#print(Haversine(47.3176006170384866,8.4482933194036445,47.4356797021342302,8.6254025463202399))
#print(distanceGeodesic(47.3176006170384866,8.4482933194036445,47.4356797021342302,8.6254025463202399))
#print(distanceCircle(47.3176006170384866,8.4482933194036445,47.4356797021342302,8.6254025463202399))
shp = fiona.open("/Users/apple/Desktop/mapdata/city_boundary/city_boundary.shp")
first=shp.next()
city=shape(first['geometry'])

print("readcity")

cen_list=generateCentroids(8.4482933194036445,47.3176006170384866,8.6254025463202399,47.4356797021342302,200)
cen_list=cleanCentroids(cen_list,city)

print("cenlist",len(cen_list),cen_list)

"""

with open('/Users/apple/Desktop/restaurants', mode='w',encoding='utf-8') as f:
	for i,j,lat,lng in cen_list:
		try:
			l=gmaps.places_nearby(location=(lat,lng),language="en",type="restaurant",radius=200)
			if l['status']=='OK':
				print(l['results'],file=f)
				print(i,j)
				while 'next_page_token' in l.keys():
					try:
						l=gmaps.places_nearby(page_token=l['next_page_token'])
						if l['status']=='OK':
							print(l['results'],file=f)
							print(i,j)
					except:
						print(i,j,lat,lng,"nextpagefailed")
						traceback.print_exc()
		except KeyboardInterrupt:
			break
		except:
			print(i,j,lat,lng)
			traceback.print_exc()
"""

"""
25 19
25 20
^Cmtec-vpn-028:code apple$ 
"""


with open('/Users/apple/Desktop/try_restaurants', mode='w',encoding='utf-8') as f:
    for i,j,lat,lng in cen_list:
        try:
            gmaps = googlemaps.Client(key='AIzaSyCoyNalHjHteQGNsvnJ4s0_yygwzoh2p20')
            l=gmaps.places_nearby(location=(lat,lng),language="en",type="restaurant",radius=200)
            getPoiModi(gmaps,l)
        del gmaps
        except KeyboardInterrupt:
            break
        except:
            print(i,j,lat,lng,"nextpagefailed")
            traceback.print_exc() 











