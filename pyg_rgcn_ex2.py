import os.path as osp

import torch
import torch.nn.functional as F
from torch_geometric.datasets import Entities
from torch_geometric.nn import RGCNConv

name = 'MUTAG'
path = osp.join(
    osp.dirname(osp.realpath(__file__)), '..', 'data', 'Entities', name)
dataset = Entities(path, name)
data = dataset[0]
print(data.num_nodes,dataset.num_relations)
print(data.edge_type,dataset.num_classes)

'''
self.conv1 = RGCNConv(
            data.num_nodes, 16, dataset.num_relations, num_bases=30)
        self.conv2 = RGCNConv(
            16, dataset.num_classes, dataset.num_relations, num_bases=30)

    def forward(self, edge_index, edge_type, edge_norm):
        x = F.relu(self.conv1(None, edge_index, edge_type))
        x = self.conv2(x, edge_index, edge_type)
        return F.log_softmax(x, dim=1)
'''


'''
Output data
#Data(edge_index=[2, 148454], edge_norm=[148454], edge_type=[148454], test_idx=[68], test_y=[68], train_idx=[272], train_y=[272])
'''

'''
Output data.test_idx
tensor([ 5836, 11133, 12995,  7224,   485, 11632, 11145, 18783, 19891, 22232,
         2528,  6714,  4825, 10051,  2458, 13340,  7212,  3314,  5786,  5735,
        13274,  3476,  8772,  3908, 17819, 19566, 20970,  7102,   598,  3365,
         3287, 18603,  4539, 13321,  5911,  4964, 18524,  7778,  3744,  7994,
        17692,   486, 17456,  4153, 23150, 22466, 22249,  4985, 14569, 12173,
        18694, 16649,  2509, 14731, 13838, 18638,  6058,  1198,   773, 23367,
         9389,   730, 13448, 18866, 12873, 16362, 19126, 13051])
'''

'''
print(data.edge_norm): 
tensor([1.0000, 1.0000, 1.0000,  ..., 1.0000, 0.5000, 1.0000])
'''

'''
tensor([[    0,     0,     0,  ..., 23643, 23643, 23643],
        [  598,  7690, 11514,  ..., 14569, 14789, 19257]])
'''

'''
print(data.num_nodes,dataset.num_relations)
print(data.edge_type,dataset.num_classes)

23644     
46
tensor([7, 8, 0,  ..., 5, 2, 0])     
2
'''
