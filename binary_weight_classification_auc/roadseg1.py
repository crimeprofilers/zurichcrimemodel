import torch
import pandas as pd
import numpy as np
from torch_geometric.data import InMemoryDataset
from torch_geometric.data import Data
from sklearn.preprocessing import LabelEncoder
from tqdm import tqdm
import torch_geometric.transforms as T

class RoadLineGraphDataset(InMemoryDataset):
    def __init__(self, root, transform=None, pre_transform=None):
        super(RoadLineGraphDataset, self).__init__(root, transform, pre_transform)
        self.data, self.slices = torch.load(self.processed_paths[0])

    @property
    def raw_file_names(self):
        #print('rawfile')
        return []
    @property
    def processed_file_names(self):
        #print('processedrawfile')
        return 'roadlinegraph_1.pt'


    def download(self):
        print('download')
        pass
    
    def process(self):
        print('start')

        df = pd.read_csv("edge_index.csv")
        target_nodes = df.road1_id.values
        source_nodes = df.road2_id.values    
        edge_index = torch.tensor([source_nodes, target_nodes], dtype=torch.long)

        f = pd.read_csv("features_4.csv")
        node_features = torch.FloatTensor(f.iloc[:,1:].values)
        print(node_features[3])

        #b = [(1 if x>0 else 0) for x in f.crime_count.values]
        #b = np.array(b)
        b_0 = [([1] if x==0 else [0]) for x in f.crime_count.values] #是否属于 no-crime
        b_1 = [([1] if x>0 else [0]) for x in f.crime_count.values] #是否属于crime
        b_0 = np.array(b_0)
        b_1 = np.array(b_1)
        b = np.concatenate((b_0,b_1),axis=1)

        #labels = torch.LongTensor(b)
        labels = torch.FloatTensor(b)
        print(b_0)
        print(b_1)
        print(labels)



        #data_list = []

        # process by session_id
        #grouped = df.groupby('session_id')
        #for session_id, group in tqdm(grouped):
        #    sess_item_id = LabelEncoder().fit_transform(group.item_id)
        #    group = group.reset_index(drop=True)
        #    group['sess_item_id'] = sess_item_id
        #node_features = group.loc[group.session_id==session_id,['sess_item_id','item_id']].sort_values('sess_item_id').item_id.drop_duplicates().values

        #node_features = torch.LongTensor(node_features).unsqueeze(1)
        #target_nodes = group.sess_item_id.values[1:]
        #source_nodes = group.sess_item_id.values[:-1]

        #edge_index = torch.tensor([source_nodes, target_nodes], dtype=torch.long)
        x = node_features
        print(x[3])
        y = labels

        '''
        print(x)
        print(x.size())
        print(y)
        print(y.size())
        print(edge_index)
        print(edge_index.size())
        '''

        #a = np.zeros(2487)
        #b = np.ones(4976)
        a = np.array([1]*4976,dtype=np.int16)
        b = np.array([2]*2487,dtype=np.int16)
        c = np.array([3]*2487,dtype=np.int16)
        print(c.dtype)
        k = np.append(a,b)

        d = np.append(k,c)
        print(d.dtype)
        
        np.random.seed(68)
        np.random.shuffle(d)
        print(d)
        split = torch.from_numpy(d)
        print(split)


        data_list = Data(x=x, edge_index=edge_index, y=y)

        data_list.train_mask = split == 1
        data_list.val_mask = split == 2
        data_list.test_mask = split == 3
        print(data_list.train_mask.sum().item())


        print(data_list.x[3])
        data_list = data_list if self.pre_transform is None else self.pre_transform(data)
        print(data_list.x[3])
        data_list = [data_list]
        print(data_list)
        print(type(data_list))
        #data_list.append(data)
    
        
        data, slices = self.collate(data_list)
        torch.save((data, slices), self.processed_paths[0])










