import torch
from roadseg1 import RoadLineGraphDataset
import os.path as osp
import torch_geometric.transforms as T
import torch.nn.functional as F
from torch.autograd import Variable
from torch_geometric.nn import GCNConv, ChebConv  # noqa
import pandas as pd
import numpy as np
from sklearn import metrics
from sklearn.metrics import roc_auc_score
from sklearn.metrics import confusion_matrix

#print("huhu")
path=osp.join(osp.dirname(osp.realpath(__file__)))
#print(path)
dataset = RoadLineGraphDataset(path,T.NormalizeFeatures())
#dataset = RoadLineGraphDataset(path)
data = dataset[0]
#print(data.contains_isolated_nodes())
#print(dataset)
#print(len(dataset))
#data.x = T.NormalizeFeatures(data.x)
#dataset.T.NormalizeFeatures()
#print(data.x[0:10])


#print(data.x[3])
#print(data.x[3].sum().item())

#print(data.x.sum().item())
#print(data.train_mask.dtype)
#print("miao")

class Net(torch.nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = GCNConv(dataset.num_features, 16, cached=True,normalize=True)
        self.conv2 = GCNConv(16, 16, cached=True,normalize=True)
        self.conv3 = GCNConv(16, 16, cached=True,normalize=True)
        self.conv4 = GCNConv(16, 16, cached=True,normalize=True)
        self.conv5 = GCNConv(16, dataset.num_classes, cached=True,normalize=True)
        #self.conv2 = GCNConv(16, 1, cached=True,normalize=True)
        #self.conv2 = GCNConv(16, 1, cached=True,normalize=True)
        # self.conv1 = ChebConv(data.num_features, 16, K=2)
        # self.conv2 = ChebConv(16, data.num_features, K=2)

        self.reg_params = self.conv1.parameters()
        self.non_reg_params = self.conv2.parameters()

    def forward(self):
        x, edge_index, edge_weight = data.x, data.edge_index, data.edge_attr
        x = F.relu(self.conv1(x, edge_index, edge_weight))
        x = F.dropout(x, training=self.training)
        x = F.relu(self.conv2(x, edge_index, edge_weight))
        x = F.relu(self.conv3(x, edge_index, edge_weight))
        x = F.relu(self.conv4(x, edge_index, edge_weight))
        x = F.dropout(x, training=self.training)
        x = self.conv5(x, edge_index, edge_weight)
        #print(x)
        #return F.log_softmax(x, dim=1)
        return x


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model, data = Net().to(device), data.to(device)
optimizer = torch.optim.Adam([
    dict(params=model.reg_params, weight_decay=5e-5),
    dict(params=model.non_reg_params, weight_decay=0)
], lr=0.1)


def train():
    model.train()
    optimizer.zero_grad()
    #F.nll_loss(model()[data.train_mask], data.y[data.train_mask]).backward()
    inpt = model()[data.train_mask]
    #print(inpt)
    #F.mse_loss(model()[data.train_mask], data.y[data.train_mask]).backward()
    #F.binary_cross_entropy_with_logits(inpt.squeeze(1),data.y[data.train_mask],pos_weight=torch.FloatTensor([3.0])).backward()
    F.binary_cross_entropy_with_logits(inpt,data.y[data.train_mask],pos_weight=torch.FloatTensor([0.1,3.0])).backward()
    optimizer.step()


def test():
    model.eval()
    logits, pres = model(), []
    recalls = []
    aucs = []
    fs = []
    for _, mask in data('train_mask', 'val_mask', 'test_mask'):

        #y = np.array([1, 1, 2, 2])
        #pred = np.array([0.1, 0.4, 0.35, 0.8])
        #fpr, tpr, thresholds = metrics.roc_curve(y, pred, pos_label=2)
        #metrics.auc(fpr, tpr)
        #y
        label = data.y[mask][:,1]
        label = label.data.numpy()
        #print(label.sum().item())
        #print(label)
        preded = logits[mask][:,1]
        preded = preded.data.numpy()
        preded = (preded+1.0)/2.0
        #print(preded)
        #fpr, tpr, thresholds = metrics.roc_curve(label, preded, pos_label=2)
        auc = roc_auc_score(label,preded)
        #auc = metrics.auc(fpr, tpr)
        #print(auc)
        aucs.append(auc)
        #tn, fp, fn, tp = confusion_matrix(label, preded).ravel()



        #pred = logits[mask].max(1)[1]
        #lab = data.y[mask].max(1)[1]
        '''
        pos=lab.sum().item() #crime的数量
        correct_prediction = pred.eq(lab) #判断正确为1，判断错误为0

        cor_pos = []
        for i in range(0,len(lab)) :
            if correct_prediction[i]==1 and lab[i]==1:
                cor_pos.append(1) #是crime为1，是no-crime为0
            else:
                cor_pos.append(0)
        cor_pos = np.array(cor_pos)

        num_recall = cor_pos.sum().item()
        recall = num_recall/pos

        #acc = pred.eq(data.y[mask]).sum().item() / mask.sum().item()
        acc = pred.eq(lab).sum().item() / mask.sum().item()
        
        pre = tp/(tp+fp)
        recall = tp/(tp+fn)
        f=2*pre*recall/(tp+fn)
        
        pres.append(pre)
        recalls.append(recall)
        '''

    #return accs+recalls+aucs
    return aucs


best_val_acc = test_acc = 0
#for epoch in range(1, 201):
pre=0
for epoch in range(1, 1001):
    train()

    #train_acc, val_acc, tmp_test_acc, train_rec, val_rec, test_rec, train_auc, val_auc, test_auc = test()
    train_auc, val_auc, test_auc = test()
    '''
    if val_acc > best_val_acc:
        best_val_acc = val_acc
        test_acc = tmp_test_acc
    '''
    
    #train_acc, val_acc, test_acc = test()

    log_1 = 'Epoch: {:03d}, Train_acc: {:.4f}, Val_acc: {:.4f}, Test_acc: {:.4f}'
    log_2 = 'Epoch: {:03d}, Train_rec: {:.4f}, Val_rec: {:.4f}, Test_rec: {:.4f}'
    log_3 = 'Epoch: {:03d}, Train_auc: {:.4f}, Val_auc: {:.4f}, Test_auc: {:.4f}'
    #print(log_1.format(epoch, train_acc, best_val_acc, test_acc))
    #print(log_2.format(epoch, train_rec, val_rec, test_rec))
    print(log_3.format(epoch, train_auc, val_auc, test_auc))

'''
#p=pre[:,1]
p=pre
p=p.detach().numpy()
p=pd.DataFrame(p)
p.to_csv('y.csv')
print(data.y[data.test_mask])
print(p)
'''
yy=data.y[data.test_mask]
yy=yy.numpy()
yy=pd.DataFrame(yy)
#yy.to_csv('y.csv')





