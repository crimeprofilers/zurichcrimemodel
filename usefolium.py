import folium
m = folium.Map(
    location=[47.390528, 8.533675],
    zoom_start=11
)

folium.GeoJson(
    '/Users/apple/Desktop/osm_zurich_roads_raw/trycleanmap.geojson',
    name='geojson'
).add_to(m)

m.render()