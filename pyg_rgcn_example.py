import os.path as osp

import torch
import torch.nn.functional as F
from torch_geometric.datasets import Entities
from torch_geometric.nn import RGCNConv

name = 'MUTAG'
path = osp.join(
    osp.dirname(osp.realpath(__file__)), '..', 'data', 'Entities', name)
print(path)
dataset = Entities(path, name)
data = dataset[0]
print(data)
#print(data.train_mask)
#print(type(data.train_mask))
print(data.test_idx)
print(type(data.test_idx))


class Net(torch.nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = RGCNConv(
            data.num_nodes, 16, dataset.num_relations, num_bases=30)
        self.conv2 = RGCNConv(
            16, dataset.num_classes, dataset.num_relations, num_bases=30)

    def forward(self, edge_index, edge_type, edge_norm):
        x = F.relu(self.conv1(None, edge_index, edge_type))
        x = self.conv2(x, edge_index, edge_type)
        return F.log_softmax(x, dim=1)


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model, data = Net().to(device), data.to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.01, weight_decay=0.0005)


def train():
    model.train()
    optimizer.zero_grad()
    out = model(data.edge_index, data.edge_type, data.edge_norm)
    F.nll_loss(out[data.train_idx], data.train_y).backward()
    optimizer.step()


def test():
    model.eval()
    out = model(data.edge_index, data.edge_type, data.edge_norm)
    pred = out[data.test_idx].max(1)[1]
    acc = pred.eq(data.test_y).sum().item() / data.test_y.size(0)
    return acc


for epoch in range(1, 51):
    train()
    test_acc = test()
    print('Epoch: {:02d}, Accuracy: {:.4f}'.format(epoch, test_acc))


'''
Traceback (most recent call last):
  File "main.py", line 7, in <module>
    RoadLineGraphDataset(path)
  File "/Users/apple/Desktop/NewCode/RGCNcode/roadseg1.py", line 11, in __init__
    super(RoadLineGraphDataset, self).__init__(root, transform, pre_transform)
  File "/Users/apple/opt/anaconda3/lib/python3.7/site-packages/torch_geometric/data/in_memory_dataset.py", line 52, in __init__
    pre_filter)
  File "/Users/apple/opt/anaconda3/lib/python3.7/site-packages/torch_geometric/data/dataset.py", line 104, in __init__
    self._process()
  File "/Users/apple/opt/anaconda3/lib/python3.7/site-packages/torch_geometric/data/dataset.py", line 148, in _process
    self.process()
  File "/Users/apple/Desktop/NewCode/RGCNcode/roadseg1.py", line 75, in process
    data, slices = self.collate(data_list)
  File "/Users/apple/opt/anaconda3/lib/python3.7/site-packages/torch_geometric/data/in_memory_dataset.py", line 122, in collate
    keys = data_list[0].keys
  File "/Users/apple/opt/anaconda3/lib/python3.7/site-packages/torch_geometric/data/data.py", line 92, in __getitem__
    return getattr(self, key, None)
TypeError: getattr(): attribute name must be string
(base) mtec-vpn-013:RGCNcode apple$ 
'''
'''
Traceback (most recent call last):
  File "main.py", line 77, in <module>
    train()
  File "main.py", line 60, in train
    F.nll_loss(model()[data.train_mask], data.y[data.train_mask]).backward()
  File "/Users/apple/opt/anaconda3/lib/python3.7/site-packages/torch/nn/functional.py", line 1838, in nll_loss
    ret = torch._C._nn.nll_loss(input, target, weight, _Reduction.get_enum(reduction), ignore_index)
RuntimeError: multi-target not supported at /Users/distiller/project/conda/conda-bld/pytorch_1573049287641/work/aten/src/THNN/generic/ClassNLLCriterion.c:22
'''







