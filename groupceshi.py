import pandas as pd
from sklearn.preprocessing import LabelEncoder

ipl_data = {'Team': ['Riders', 'Riders', 'Devils', 'Devils', 'Kings',
         'kings', 'Kings', 'Kings', 'Riders', 'Royals', 'Royals', 'Riders'],
         'Rank': [1, 2, 2, 3, 3,4 ,1 ,1,2 , 4,1,2],
         'Year': [2014,2015,2014,2015,2014,2015,2016,2017,2016,2014,2015,2017],
         'Points':[876,789,863,673,741,812,756,788,694,701,804,690]}
df = pd.DataFrame(ipl_data)

grouped = df.groupby('Year')

for name,group in grouped:
	print (name)
	print (group)
	group_team_id = LabelEncoder().fit_transform(group.Team)
	group = group.reset_index(drop=True)
	group['group_team_id'] = group_team_id
	print (group)
	node_features = group.loc[group.Year==name,['group_team_id','Team']].sort_values('group_team_id').Team.drop_duplicates().values
	print(node_features)
	#node_features = torch.LongTensor(node_features).unsqueeze(1)
	target_nodes = group.group_team_id.values[1:]
	source_nodes = group.group_team_id.values[:-1]
	print (target_nodes)
	print (source_nodes)
    #edge_index = torch.tensor([source_nodes, target_nodes], dtype=torch.long)
    #x = node_features
    #y = torch.FloatTensor([group.label.values[0]])


    #print (name)
    #print (group)
    #print (type(group))
    #kk = group.loc[group.Year==name,'Points']
    #print (kk.values)
    #print (group.loc[:,'Points'].values)
    #print (type(kk))
    #原文出自【易百教程】，商业转载请联系作者获得授权，非商业请保留原文链接：https://www.yiibai.com/pandas/python_pandas_groupby.html

