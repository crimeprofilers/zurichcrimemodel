import torch
from roadseg1 import RoadLineGraphDataset
import os.path as osp
import torch_geometric.transforms as T
import torch.nn.functional as F
from torch_geometric.nn import GCNConv, ChebConv  # noqa

#print("huhu")
path=osp.join(osp.dirname(osp.realpath(__file__)))
#print(path)
dataset = RoadLineGraphDataset(path,T.NormalizeFeatures())
#dataset = RoadLineGraphDataset(path)
data = dataset[0]
print(data.contains_isolated_nodes())
print(dataset)
print(len(dataset))
#data.x = T.NormalizeFeatures(data.x)
#dataset.T.NormalizeFeatures()
#print(data.x[0:10])


print(data.x[3])
print(data.x[3].sum().item())

print(data.x.sum().item())
print(data.train_mask.dtype)
#print("miao")


class Net(torch.nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = GCNConv(dataset.num_features, 16, cached=True,normalize=True)
        #self.conv2 = GCNConv(16, dataset.num_classes, cached=True,normalize=True)
        self.conv2 = GCNConv(16, 1, cached=True,normalize=True)
        # self.conv1 = ChebConv(data.num_features, 16, K=2)
        # self.conv2 = ChebConv(16, data.num_features, K=2)

        self.reg_params = self.conv1.parameters()
        self.non_reg_params = self.conv2.parameters()

    def forward(self):
        x, edge_index, edge_weight = data.x, data.edge_index, data.edge_attr
        x = F.relu(self.conv1(x, edge_index, edge_weight))
        x = F.dropout(x, training=self.training)
        x = self.conv2(x, edge_index, edge_weight)
        #print(x)
        #return F.log_softmax(x, dim=1)
        return x


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model, data = Net().to(device), data.to(device)
optimizer = torch.optim.Adam([
    dict(params=model.reg_params, weight_decay=5e-5),
    dict(params=model.non_reg_params, weight_decay=0)
], lr=0.01)


def train():
    model.train()
    optimizer.zero_grad()
    #F.nll_loss(model()[data.train_mask], data.y[data.train_mask]).backward()
    F.mse_loss(model()[data.train_mask], data.y[data.train_mask]).backward()
    optimizer.step()


def test():
    model.eval()
    logits, accs = model(), []
    for _, mask in data('train_mask', 'val_mask', 'test_mask'):
        #pred = logits[mask].max(1)[1]
        #acc = pred.eq(data.y[mask]).sum().item() / mask.sum().item()
        acc=F.mse_loss(logits[mask],data.y[mask])
        print(acc)
        accs.append(acc)
    return accs


best_val_acc = test_acc = 0
#for epoch in range(1, 201):
for epoch in range(1, 50):
    train()
    '''
    train_acc, val_acc, tmp_test_acc = test()
    if val_acc > best_val_acc:
        best_val_acc = val_acc
        test_acc = tmp_test_acc
    '''
    train_acc, val_acc, test_acc = test()

    log = 'Epoch: {:03d}, Train: {:.4f}, Val: {:.4f}, Test: {:.4f}'
    print(log.format(epoch, train_acc, best_val_acc, test_acc))


