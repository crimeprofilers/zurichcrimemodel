BUILD INTERSECTIONS


#!/usr/bin/python3
# -*- coding: utf-8 -*-
 
import psycopg2, sys, os, time
from shapely.geometry import LineString
from shapely import wkb
 
debug = 0
t = time.process_time()
minRoad = 0
 
ipoints = {}
i2r = []
 
def connectDB():
  try:
    conn = psycopg2.connect("dbname='shared' user='ssandow' host='localhost' password='gvc4XVlIU8'")
  except Exception as e:
    print("I am unable to connect to the database"+str(e))
    sys.exit(1)
  return conn
 
def createIntersections(r):
    # Begin and End points
    global myIntersectId
    for point in [str(r[1]),str(r[2])]:
        success=0
        if point in ipoints.keys():
            intersect_id = ipoints[point]
            i2r.append([intersect_id,r[0]])
            successCount['entityRelation']+=1
        else:
            successCount['intersect']+=1
            successCount['entityRelation']+=1
            ipoints[point] = myIntersectId
            i2r.append([myIntersectId,r[0]])
            myIntersectId += 1
           
    
def getAllRoads():
    myIntersectId = 1
    roadCurs = conn.cursor()
    sql = "select gid,ST_Startpoint(geom),ST_Endpoint(geom) from open.nyc_road_proj_final"
    roadCurs.execute(sql)
    res = [1]
   
    while (len(res) >=1):
        res = roadCurs.fetchmany(1000)
        #print("Time: {0}, RoadId: {1},".format(time.process_time() -t,res[0][0]))
        if len(res)>0:
            if (res[0][0] >= minRoad):
                for r in res:
                    successCount['road'] += 1
                    createIntersections(r)
        if debug:
            print('Debug is active. Ending now.')
            res=[]
 
        print("Success counters: {0}".format(str(successCount)))
    for l in i2r:
        filei2r.write("{},{}\n".format(l[0],l[1]))
    for i in ipoints.keys():
        fileintersect.write("{},{}\n".format(i,ipoints[i]))
 
filei2r = open('i2r_3.sql','w')
fileintersect = open('intersect_3.sql','w')
myIntersectId = 1
successCount = {'road':0,'intersect':0,'entityRelation':0}
conn = connectDB()
getAllRoads()
 
 
 
 
BUILD ROAD NETWORK
 
def createRoadNetwork(self):
        # SQL to select data
        # from intersection2road table as i2r: intersection_id [key]
        # from open.nyc_road_proj_final as r: gid [1]
        # from open.nyc_road_attributes as ra: length [2], crimes_2015 [3]
        # join tables using road_id into gid
       
        roadLength=0
        weightdict=dict()
        #crimes_2015: n crime to 1 road mapping
        #open.nyc_road_attributes without and open.nyc_road_attributes2 with census tract for each road
        self.mycurs.execute("""select intersection_id,r.gid, length, ra.st_width, roadtypew, crimes_2015, censustract from
            open.nyc_intersection2road i2r
            left join open.nyc_road_proj_final r on i2r.road_id = r. gid
            left join open.nyc_road_attributes2 ra on ra.road_id=r.gid""")
        # total attributes: 148466 roads; st_width=0 '6061' --> 4%
        #fetch all values into tuple
        interRoad=self.mycurs.fetchall()
        #dictionary {} -  a Key and a value, in this case a key and a set() of values -unordered collection
       
        #for each line in interRoad
        for line in interRoad:
            #if attribute[0] (intersection_id) is not in intersect
            if not line[0] in self.intersect:
                #initialize a set for the key (intersect_id)
                self.intersect[line[0]]=set()
            #add current road_id to key
            self.intersect[line[0]].add(line[1])
        #for G2
        for line in interRoad:
            if not line[1] in self.roads:
                self.roads[line[1]]=set()
            self.roads[line[1]].add(line[0])           
            #add road, length and crimes as node info in graph
            #self.G.add_node(line[1], length=line[2], num_crimes=line[3])
            # TODO Parameter: Assumptions Humans walk 300 feet in 60s
            self.G.add_node(line[1],length=line[2], width=line[3], roadtype=line[4], census=line[6])
            self.G2.add_node(line[0])
            weightdict[line[1]]=[line[2], line[3], line[4], line[6]]
        #for r in self.G.nodes_iter():
            #roadLength+=self.G.node[r]['length']
        #self.log.debug("Found {} intersections".format(len(intersect)))
        #self.log.debug("roadlenght: {}".format(roadLength))
        #build edges with information on nodes (roads)
        # loops over each intersection in intersect[]    
        for interKey in self.intersect.keys():
            #loops over each road in the current intersection
            for road in self.intersect[interKey]:
                #loops over roads again to compare roads and map relationship
                for road2 in self.intersect[interKey]:
                    if not road==road2:
                        self.G.add_edge(road, road2)
        #for G2
        for roadKey in self.roads.keys():
            #loop over each intersection in roads
            for inters in self.roads[roadKey]:
                #loop over intersection again to compare intersect and map relationship
                for inters2 in self.roads[roadKey]:
                    if not inters==inters2:
                        self.G2.add_edge(inters, inters2, length=weightdict[roadKey][0], width=weightdict[roadKey][1], lengthwidth=((10*weightdict[roadKey][0])*weightdict[roadKey][1]), roadtype=weightdict[roadKey][2], roadtypelength=(weightdict[roadKey][0]*weightdict[roadKey][2]), census=weightdict[roadKey][3])
 
        self.log.debug("Number of  G: roads: {0}, intersections: {1}".format(self.G.number_of_nodes(), self.G.number_of_edges))
        self.log.debug("Number of  G2: roads: {1}, intersections: {0}".format(self.G2.number_of_nodes(), self.G2.number_of_edges))
        #self.log.debug("Isolated roads: {0}".format(len(nx.isolates(self.G))))
        #self.log.info("roadNW built, intersection size: {0}".format(len(intersect)))
        #self.log.info("roadNW built, roads size: {0}".format(self.G.number_of_nodes()))
       
        
        """     
        117320  total   100%
        115694  minus isolates  98.61%
        113182  minus detached  96.47%
        """
        self.mycurs.execute("""select * from open.nyc_road_proj_final_isolates""")
        #fetch all values into tuple
        globalVar.isolateRoadsRNW.update(self.mycurs.fetchall())
 
        #print(self.G.node[5134]['length'])
        #print(self.G.node[5134]['width'])
        #output is not a list of roads, but list of ('road_id')